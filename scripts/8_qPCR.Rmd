---
title: "8_qPCR"
author: "Emma de Koff"
date: "19-1-2022"
output: html_document
---

```{r setup, include=FALSE, message = FALSE}
library(here); library(phyloseq); library(tidyverse); library(magrittr); library(EnvStats); library(ggpubr); library(DescTools); library(RColorBrewer); library(glue);
library(ggtext); library(ggforce); library(readxl); library(panelr); library(Hmisc); library(tableone)

subdir_name <- "8_qPCR"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here::here("results", "figures", glue("{subdir_name}//")), dpi=300)
#theme_set(theme_bw()) # global option for ggplot2

```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input=here::here("scripts", paste0(subdir_name, ".Rmd")), output_dir=here::here("results"))
```

# load data
```{r}
# qpCR results
data_qpcr_raw <- read_excel(here("data", "raw", "KA data 8-2-2019 Total overview result qPCR VETMAX_adapted.xlsx"))

# physeq
load(here("data", "processed", "ps_vacc.RData"))
# custom functions
source(here("functions", "utils.R"))
```

# Excel to long format
```{r}
data_qpcr_m1 <- data_qpcr_raw %>%
  fill(Sample, .direction = "down") %>%
  select(Sample, ends_with("M1")) %>% 
  set_colnames(., str_remove(colnames(.), "_M1"))

data_qpcr_m2 <- data_qpcr_raw %>%
  fill(Sample, .direction = "down") %>%
  select(Sample, ends_with("M2")) %>% 
  set_colnames(., str_remove(colnames(.), "_M2"))

data_qpcr_m3 <- data_qpcr_raw %>%
  fill(Sample, .direction = "down") %>%
  select(Sample, ends_with("M3")) %>% 
  set_colnames(., str_remove(colnames(.), "_M3"))

data_qpcr_m4 <- data_qpcr_raw %>%
  fill(Sample, .direction = "down") %>%
  select(Sample, ends_with("M4")) %>% 
  set_colnames(., str_remove(colnames(.), "_M4"))


data_qpcr_ad <- rbind(data_qpcr_m1, data_qpcr_m2, data_qpcr_m3, data_qpcr_m4) %>%
  filter(Target %in% c("Enterococcus spp.", "E. coli", "Klebsiella spp."),
         Sample != "121142010731") %>%  # 1 sample discarded 
  pivot_wider(id_cols = "Sample", names_from = "Target", values_from = c("Ct", "Target_Result", "Custom_Call")) %>%
  mutate(across(starts_with("Target_Result"), ~ ifelse(.x == "POSITIVE", "1", "0") %>% factor)) 

```

# Add physeq data
```{r}
df_qpcr_vacc <- ps$RA %>%
  subset_samples(Timepoint == "42") %>%
  ps_to_df() %>% 
  select(Sample.name, Escherichia_coli_2, Enterococcus_faecium_5, Klebsiella_4, qpcr_16s_pgul,
         average_Ps6b_12m, Ps6b_med_cat, average_menc_18m, menc_med_cat, pp_alg_utn_v, d_pcv11m_hv12m, d_menc_hv18m) %>%
  mutate(across(c(Escherichia_coli_2, Enterococcus_faecium_5, Klebsiella_4), ~ multiply_by(.x, qpcr_16s_pgul), .names = "{.col}_abs")) %>%
  left_join(data_qpcr_ad, ., by = c("Sample" = "Sample.name"))

```

# Chisquare tests
```{r}
# with Ps6b cat
df_qpcr_vacc %>% drop_na(Ps6b_med_cat) %>% group_by(`Target_Result_E. coli`, Ps6b_med_cat) %>% count
df_qpcr_vacc %>% drop_na(Ps6b_med_cat) %>% group_by(`Target_Result_Enterococcus spp.`, Ps6b_med_cat) %>% count
df_qpcr_vacc %>% drop_na(Ps6b_med_cat) %>% group_by(`Target_Result_Klebsiella spp.`, Ps6b_med_cat) %>% count

with(df_qpcr_vacc, chisq.test(`Target_Result_E. coli`, Ps6b_med_cat))
with(df_qpcr_vacc, chisq.test(`Target_Result_Enterococcus spp.`, Ps6b_med_cat))
with(df_qpcr_vacc, chisq.test(`Target_Result_Klebsiella spp.`, Ps6b_med_cat))
# trend for E. coli

# with MenC cat
df_qpcr_vacc %>% drop_na(menc_med_cat) %>% group_by(`Target_Result_E. coli`, menc_med_cat) %>% count
df_qpcr_vacc %>% drop_na(menc_med_cat) %>% group_by(`Target_Result_Enterococcus spp.`, menc_med_cat) %>% count
df_qpcr_vacc %>% drop_na(menc_med_cat) %>% group_by(`Target_Result_Klebsiella spp.`, menc_med_cat) %>% count

with(df_qpcr_vacc, chisq.test(`Target_Result_E. coli`, menc_med_cat))
with(df_qpcr_vacc, chisq.test(`Target_Result_Enterococcus spp.`, menc_med_cat))
with(df_qpcr_vacc, chisq.test(`Target_Result_Klebsiella spp.`, menc_med_cat))
# none significant

# with mode of birth
df_qpcr_vacc %>% drop_na(pp_alg_utn_v) %>% group_by(`Target_Result_E. coli`, pp_alg_utn_v) %>% count
df_qpcr_vacc %>% drop_na(pp_alg_utn_v) %>% group_by(`Target_Result_Enterococcus spp.`, pp_alg_utn_v) %>% count
df_qpcr_vacc %>% drop_na(pp_alg_utn_v) %>% group_by(`Target_Result_Klebsiella spp.`, pp_alg_utn_v) %>% count

with(df_qpcr_vacc, chisq.test(`Target_Result_E. coli`, pp_alg_utn_v))
with(df_qpcr_vacc, chisq.test(`Target_Result_Enterococcus spp.`, pp_alg_utn_v))
with(df_qpcr_vacc, chisq.test(`Target_Result_Klebsiella spp.`, pp_alg_utn_v))
# trend for E. coli

# Create Table
CreateTableOne(vars = c("Target_Result_E. coli", "Target_Result_Klebsiella spp.", "Target_Result_Enterococcus spp."),
               strata = c("Ps6b_med_cat"),
               data = df_qpcr_vacc) %>%
  print() %>%
  as.data.frame %>%
  select(-test, Ps6B_hi = hi, Ps6B_lo = lo, Ps6B_p = p) %>%
  rownames_to_column("qPCR_target") %>%
left_join(., CreateTableOne(vars = c("Target_Result_E. coli", "Target_Result_Klebsiella spp.", "Target_Result_Enterococcus spp."),
               strata = c("menc_med_cat"),
               data = df_qpcr_vacc) %>%
  print() %>%
  as.data.frame %>%
  select(-test, MenC_hi = hi, MenC_lo = lo, MenC_p = p) %>%
  rownames_to_column("qPCR_target"), by = "qPCR_target") %>%
  write.table(., file = here("results", subdir_name, "qPCR_validation.txt"), quote = F, row.names = F, sep = ";")
  
  

```

# Correlations Ct value with absolute abundance
```{r}
# negative results were assigned 0 Ct-value, change to high number 45
df_qpcr_vacc_rep <- df_qpcr_vacc %>% 
     mutate(`Ct_E. coli` = ifelse(`Ct_E. coli` == 0, 45, `Ct_E. coli`),
            `Ct_Enterococcus spp.` = ifelse(`Ct_Enterococcus spp.` == 0, 45, `Ct_Enterococcus spp.`),
            `Ct_Klebsiella spp.` = ifelse(`Ct_Klebsiella spp.` == 0, 45, `Ct_Klebsiella spp.`))

# E. coli absolute abundance
df_qpcr_vacc_rep %>%
  select(`Ct_E. coli`, Escherichia_coli_2_abs) %>%
  mutate(Escherichia_coli_2_abs = Escherichia_coli_2_abs / 7) %>% # corrects for copy number differences, but should not matter
  #filter(`Ct_E. coli` > 0) %>%
  as.matrix %>%
  rcorr(type = "spearman")

# E. coli relative abundance
df_qpcr_vacc_rep %>%
  select(`Ct_E. coli`, Escherichia_coli_2) %>%
  #filter(`Ct_E. coli` > 0) %>%
  as.matrix %>%
  rcorr(type = "spearman")

# Enterococcus absolute abundance
df_qpcr_vacc_rep %>%
  select(`Ct_Enterococcus spp.`, Enterococcus_faecium_5_abs) %>%
  mutate(Enterococcus_faecium_5_abs = Enterococcus_faecium_5_abs / 6) %>% 
  as.matrix %>%
  rcorr(type = "spearman")

# Enterococcus relative abundance
df_qpcr_vacc_rep %>%
  select(`Ct_Enterococcus spp.`, Enterococcus_faecium_5) %>%
  as.matrix %>%
  rcorr(type = "spearman")

# Klebsiella absolute abundance
df_qpcr_vacc_rep %>%
  select(`Ct_Klebsiella spp.`, Klebsiella_4_abs) %>%
  mutate(Klebsiella_4_abs = Klebsiella_4_abs / 8) %>% 
  as.matrix %>%
  rcorr(type = "spearman")

# Klebsiella relative abundance
df_qpcr_vacc_rep %>%
  select(`Ct_Klebsiella spp.`, Klebsiella_4) %>%
  as.matrix %>%
  rcorr(type = "spearman")



```
# Linear model ct value and IgG responses
```{r}
lm(log(average_Ps6b_12m) ~ `Ct_E. coli` + poly(d_pcv11m_hv12m, 2), data = df_qpcr_vacc_rep %>% drop_na(d_pcv11m_hv12m)) %>% summary
lm(log(average_menc_18m) ~ `Ct_E. coli` + poly(d_menc_hv18m, 2), data = df_qpcr_vacc_rep %>% drop_na(d_menc_hv18m)) %>% summary

lm(log(average_Ps6b_12m) ~ `Target_Result_E. coli` + poly(d_pcv11m_hv12m, 2), data = df_qpcr_vacc_rep %>% drop_na(d_pcv11m_hv12m)) %>% summary
lm(log(average_menc_18m) ~ `Target_Result_E. coli` + poly(d_menc_hv18m, 2), data = df_qpcr_vacc_rep %>% drop_na(d_menc_hv18m)) %>% summary

```



```{r}
sessionInfo()
```

