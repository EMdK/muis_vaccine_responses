---
title: "3_meta_influences"
author: "Emma de Koff"
date: "29-7-2020"
output: 
  html_document:
    toc: yes
    toc_float: true
    code_folding: hide
---

```{r setup, include=FALSE}
# load libraries
library(here); library(phyloseq); library(tidyverse); library(magrittr); library(reshape2); library(ggpubr); library(grid); library(gridExtra); library(knitr); library(DescTools); library(glue); library(lme4); library(lmerTest); library(tibble); library(multcomp); library(RColorBrewer); library(janitor) ; library(tableone); library(broom); library(EnvStats); library(agricolae); library(ggtext)

subdir_name <- "3_meta_influences"

# set paths
knitr::opts_knit$set(root.dir=".", aliases=c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(dev=c('png', 'pdf'), 
                      fig.path=here::here("results", "figures", glue("{subdir_name}//")), dpi=300)
#theme_set(theme_bw()) # global option for ggplot2

```

```{r knit, echo=F, eval=FALSE}
rmarkdown::render(input=here::here("scripts", paste0(subdir_name, ".Rmd")), output_dir=here::here("results"))
```

# Functions
```{r}
# function to compute geometric mean and 95% CI
geo.ci.fun <- function(x) {
  r <- exp(MeanCI(log(x)))
  r = c(r[2], exp(mean(log(x))), r[3])
  names(r) <- c("ymin", "y", "ymax") 
  r 
}

# assign inside chain
put <- function(x, name, where = NULL) {
  name_string <- deparse(substitute(name))
  if (is.null(where)) {
    sys_calls <- sys.calls()
    put_calls <- grepl("\\<put\\(", sys_calls) & !grepl("\\<put\\(\\.",sys_calls)
    where <- sys.frame(max(which(put_calls)) - 1)
  }
  assign(name_string, value = x, pos = where)
}

# smooth
s <- function(vec, digits = 3, format = "f", flag = ""){
  return(gsub("\\.$", "", 
              formatC(vec, 
                      digits = digits, 
                      # use "fg" for significant digits
                      format = format, 
                      flag = flag)))
}

# write source data to Excel
write_source_data <- function(data, name, panel) {
  
  # check folder 
  if(!fs::dir_exists(here("results", "source_data", "by_panel"))) {
    fs::dir_create(here("results", "source_data", "by_panel"))
  }
  
  # write data
  openxlsx::write.xlsx(
    data, 
    file = here("results", "source_data", "by_panel", glue::glue("Source_Data_{name}{panel}.xlsx")), 
    overwrite = TRUE)
}
```

# Data
```{r}
# phyloseq with metadata
load(here::here("data", "processed", "ps_vacc.RData"))

# vaccination data
load(here::here("data", "processed", "vacc_data.RData"))

#colnames(sample_data(ps$RA) %>% as.data.frame)
```

# Metadata dataframe
```{r}
metaMUIS_long <- get_variable(ps$RA, c("dn_nr", "Timepoint", "pp_alg_utn_v", "bf1m", "bf3m", "BV_dagen", "bottle_excl", "gez_medicatie_ab", "AB", "pet", "fg_sibs_aant", "daycare_ever", "daycare_since", "dn_dd_geslacht_kind", "AB_courses", "AB_courses_m18"))

# first create variables that are clear and apply to every child: 
## for AB: create vectors with participant numbers who had AB before certain timepoint
dn_ab_m1 <- metaMUIS_long$dn_nr[which(metaMUIS_long$Timepoint %in% c("41","42","43","44") & metaMUIS_long$gez_medicatie_ab == "1")] %>% unique
dn_ab_m3 <- c(metaMUIS_long$dn_nr[which(metaMUIS_long$Timepoint %in% c("41","42","43","44", "45", "46") & metaMUIS_long$gez_medicatie_ab == "1")], "K-016", "K-065", "K-069", "K-073", "K-085") %>% unique ## manually added mo 3 ab users from database because this timepoint is missing from the physeq
dn_ab_m4 <- c(metaMUIS_long$dn_nr[which(metaMUIS_long$Timepoint %in% c("41","42","43","44", "45", "46","47") & metaMUIS_long$gez_medicatie_ab == "1")], "K-016", "K-065", "K-069", "K-073", "K-085") %>% unique ## manually added mo 3 ab users from database because this timepoint is missing from the physeq
dn_ab_m6 <- c(metaMUIS_long$dn_nr[which(metaMUIS_long$Timepoint %in% c("41","42","43","44", "45", "46", "47", "48") & metaMUIS_long$gez_medicatie_ab == "1")], "K-016", "K-065", "K-069", "K-073", "K-085") %>% unique ## manually added mo 3 ab users from database because this timepoint is missing from the physeq

# prenatal antibiotics: in 3 infants (see MUIS-5 metadata R project, codebook, `pp_prt_ab_prenat`) - M018,M027,M121 due to maternal fever
dn_ab_prenat <- c("K-018", "K-027", "K-121")

metaMUIS <- metaMUIS_long %>%
  mutate(vag_b = pp_alg_utn_v %>% factor(),
         sex = dn_dd_geslacht_kind %>% factor(),     # 1 = boy , 2 = girl
         bf1m = bf1m %>% factor(),
         bf3m = bf3m %>% factor(),
         bottle_excl = bottle_excl %>% factor(),
         ab_m1 = case_when(dn_nr %in% dn_ab_m1 ~ 1,
                           TRUE ~ 0) %>% factor(),
         ab_m3 = case_when(dn_nr %in% dn_ab_m3 ~ 1,
                           TRUE ~ 0) %>% factor(),
         ab_m4 = case_when(dn_nr %in% dn_ab_m4 ~ 1,
                           TRUE ~ 0) %>% factor(),
         ab_m6 = case_when(dn_nr %in% dn_ab_m6 ~ 1,
                           TRUE ~ 0) %>% factor(),
         ab_y1 = AB %>% factor(),
         ab_prenat = case_when(dn_nr %in% dn_ab_prenat ~ 1,
                               TRUE ~ 0) %>% factor,
         pets = case_when(pet == "none" ~ 0,
                          is.na(pet) ~ NA_real_,
                          TRUE ~ 1) %>% factor(),
         cat = case_when(pet == "cat(s)" | pet == "cat(s) + dog(s)" ~ 1,
                         is.na(pet) ~ NA_real_,
                         TRUE ~ 0) %>% factor(),
         dog = case_when(pet == "dog(s)" | pet == "cat(s) + dog(s)" ~ 1,
                         is.na(pet) ~ NA_real_,
                         TRUE ~ 0) %>% factor(),
         sibs = case_when(fg_sibs_aant >=1 ~ 1,
                          fg_sibs_aant == 0 ~ 0,
                          TRUE ~ NA_real_) %>% factor(),
         n_sibs = fg_sibs_aant %>% as.numeric(),
         dc_m3 = case_when(daycare_since %in% c("m2", "m3") ~ 1,
                           daycare_since %in% c("m4", "m6", "m9", "m12", ">m12") ~ 0,
                           TRUE ~ NA_real_) %>% factor(),
         dc_m6 = case_when(daycare_since %in% c("m2", "m3", "m4", "m6")~ 1,
                           daycare_since %in% c("m9", "m12", ">m12") ~ 0,
                           TRUE ~ NA_real_) %>% factor()) %>%
  dplyr::select(dn_nr, Timepoint, vag_b, sex, bf1m, bf3m, BV_dagen, bottle_excl, ab_m1, ab_m3, ab_m4, ab_m6, ab_y1, ab_prenat, pets, cat, dog, sibs, n_sibs, daycare_ever, dc_m3, dc_m6, AB_courses, AB_courses_m18) %>%
  # since all variables are now per child and month 1 is complete, we can filter on timepoint
  # check: length mo 1 = 120, 3 kids who dropped out after month 1 and thus only have baseline measurement are not in it -> no problem
  filter(Timepoint == "44") %>% 
  dplyr::select(-Timepoint)
```

## Cohort description
```{r tables}
check1 <- nrow(metaMUIS) == 120

metaMUIS %>% pull(sex) %>% summary
# 57 male, 48%

metaMUIS %>% pull(vag_b) %>% summary
# 46 c-section, 38%

metaMUIS %>% pull(bottle_excl) %>% summary
# 22 never breastfed, 18% 

# infants with pneumo response available
CreateTableOne(vars = c("sex", "vag_b", "ab_prenat", "bottle_excl", "BV_dagen", "sibs", "n_sibs", "pets", "ab_m3", "ab_y1", "AB_courses"),
               data = metaMUIS %>% filter(dn_nr %in% (vacc_igg %>% drop_na(average_Ps1_12m) %>% pull(dn_nr)))) %>%
  print(nonnormal = c("BV_dagen", "n_sibs", "AB_courses")) %>%
  as.data.frame %>%
  rownames_to_column("var") %>%
  write_delim(here::here("results", subdir_name, "tab1_m12.txt"), delim = ";")

#infants with menc response available
CreateTableOne(vars = c("sex", "vag_b", "ab_prenat", "bottle_excl", "BV_dagen", "sibs", "n_sibs", "pets", "ab_m3", "ab_y1", "AB_courses_m18"),
               data = metaMUIS %>% filter(dn_nr %in% (vacc_igg %>% drop_na(average_menc_18m) %>% pull(dn_nr)))) %>%
  print(nonnormal = c("BV_dagen", "n_sibs", "AB_courses_m18")) %>%
  as.data.frame %>%
  rownames_to_column("var") %>%
  write_delim(here::here("results", subdir_name, "tab1_subset_m18.txt"), delim = ";")
  
metaMUIS %>%
  dplyr::select(dn_nr, sex, vag_b, ab_prenat, bottle_excl, BV_dagen, sibs, n_sibs, pets, ab_m3, ab_y1, AB_courses, AB_courses_m18) %>%
  write_source_data("Table1", "")
```

## Diagram with number of samples per timepoint
```{r flowchart,w=7.5, h=2}
df_n <- rbind(ps$RA %>% 
  sample_data() %>% as.data.frame %>%
  group_by(Timepoint) %>%
  summarise(n=n()) %>%
  mutate(dataset="Gut microbiota \n(full cohort)",
         N=length(unique(ps$RA %>% sample_data() %>% as.data.frame %>% pull(dn_nr))),
         n_total = 1156,
         n_total_lab="1,156"),

  data.frame(n= nrow(vacc_igg %>%
  drop_na(average_Ps1_12m))) %>%
  mutate(Timepoint = "50",
    dataset="Salivary \nanti-pneumococcal IgG",
         N=118,
    n_total = 118,
         n_total_lab="118"),
  
  data.frame(n= nrow(vacc_igg %>%
  drop_na(average_menc_18m))) %>%
  mutate(Timepoint = "51",
         dataset="Salivary \nanti-meningococcal IgG",
         N=78,
         n_total = 78,
         n_total_lab="78")) %>%
  mutate(size_n = log(n, base=2),
         dataset=fct_inorder(dataset)%>% fct_rev)


dot_tp <- df_n %>%
  ggplot(aes(x=Timepoint, y=dataset, size=size_n)) +
  geom_point(aes(fill=dataset), color = "grey10", shape=21, alpha=0.95) +
  scale_size(range=c(0,10), limits=c(0, NA)) +
  guides(size="none") +
  scale_fill_manual(values=brewer.pal(4, "RdGy")) +
  scale_x_discrete(name="time point", position = "top", labels = c("d0", "d1", "w1", "w2", "m1", "m2", "m4", "m6", "m9", "m12", "m18")) +
theme_bw() +
  theme(panel.grid.major = element_blank(), legend.position = "none") +
  geom_text(aes(label=n), color="white", size=3) +
  labs(y="") 

dot_N <- df_n %>%
  filter(!duplicated(N)) %>%
  mutate(x="x") %>%
  ggplot(aes(x=x,y=dataset, size=log(N, base=2))) +
  geom_point(aes(fill=dataset), color = "grey10", shape=21, alpha=0.95) +
  scale_size(range=c(0,10), limits=c(0, NA)) +
  guides(size="none") +
  scale_fill_manual(values=brewer.pal(4, "RdGy")) +
  scale_x_discrete(name="", label = "N", position = "top") +
  theme_bw() +
  theme(panel.grid.major = element_blank(), legend.position = "none") +
  geom_text(aes(label=N), color="white", size=3) + 
  labs(y="") 

dot_n_total <- df_n %>%
  filter(!duplicated(n_total)) %>%
  mutate(x="x") %>%
  ggplot(aes(x=x, y=dataset, size=log(n_total, base=2))) +
  geom_point(aes(fill=dataset), color = "grey10", shape=21, alpha=0.95) +
  scale_size(range=c(0,10), limits=c(0, NA)) +
  guides(size="none") +
  scale_fill_manual(values=brewer.pal(4, "RdGy")) +
  scale_x_discrete(name="", label = "n", position = "top") +
  theme_bw() +
  theme(panel.grid.major = element_blank(), legend.position = "none") +
  geom_text(aes(label=n_total_lab), color="white", size=3) + 
  labs(y="") 

dot_tp_adj <- dot_tp + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank()) # , axis.title.x = element_blank()
dot_n_total_adj <- dot_n_total + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank())

# separate plots
dot_tp
dot_N
dot_n_total

# combine
cowplot::plot_grid(dot_N, dot_n_total_adj, dot_tp_adj, nrow=1, rel_widths = c(0.3, 0.125, 0.7))

df_n %>%
  write_source_data("SupplFig1", "")
```

# Metadata and vaccination data, dataframes
```{r}
# MenC
meta_menc_df <- merge(metaMUIS, menc_vacc_igg, by = "dn_nr") %>%
  filter(vac_14m_menc == "yes")

# log-transformed IgG, long format
meta_logmenc_df <- meta_menc_df %>%
  mutate(log_menc_18m = log1p(average_menc_18m)) %>%
  dplyr::select(-average_menc_18m)  %>%
  mutate(bottle_excl2 = ifelse(BV_dagen < 5, "1", "0") %>% factor)

# Pneu
meta_pcv_df <- merge(metaMUIS, pcv_vacc_igg, by = "dn_nr")

# log-transformed IgG, long format
#meta_logpcv_df <- 
meta_logpcv_df <- meta_pcv_df %>%
  dplyr::select(-grep("Ps1_|Ps3|Ps4|Ps5|Ps6a|Ps9|Ps18|Ps19|Ps23", colnames(.))) %>%
  mutate(log_ps6b_12m = log1p(average_Ps6b_12m),
         log_ps7f_12m = log1p(average_Ps7f_12m),
         log_ps14_12m = log1p(average_Ps14_12m)) %>%
  mutate(bottle_excl2 = ifelse(BV_dagen < 5, "1", "0") %>% factor(),
         #vag_b = relevel(vag_b, ref = "1")
         ) %>%
  dplyr::select(-grep("average", colnames(.))) 
  
```


## Univariable models
```{r}
# MenC
sapply(c("vag_b", "sex", "bf1m", "bf3m", "bottle_excl", "bottle_excl2", "BV_dagen", "ab_m1", "ab_m3", "ab_m6", "ab_y1", "AB_courses_m18", "pets", "cat", "dog", "sibs", "n_sibs", "daycare_ever", "dc_m3"), function(x){ 
  lm_uni <- lm(as.formula(glue("log_menc_18m ~ {x}")), data = meta_logmenc_df) 
  pval <- coef(summary(lm_uni))[8] %>% round(digits = 3)
  est <- coef(summary(lm_uni))[2] %>% round(digits = 3)
  list("pval" = pval,
       "est" = est)},
  USE.NAMES = TRUE, simplify = TRUE) %>%
  kable

# Pneumo
sapply(c("average_Ps1_12m", "average_Ps4_12m", "average_Ps5_12m", "average_Ps7f_12m", "average_Ps9v_12m",
         "average_Ps14_12m", "average_Ps18c_12m", "average_Ps19f_12m", "average_Ps23f_12m"), function(RESP) {
sapply(c("vag_b", "sex", "bf1m", "bf3m", "bottle_excl", "BV_dagen", "ab_m1", "ab_m3", "ab_m6", "ab_y1", "AB_courses_m18", "pets", "cat", "dog", "sibs", "n_sibs", "daycare_ever", "dc_m3"), function(x){ 
  df_uni <- meta_pcv_df %>% dplyr::select(RESP, x) %>% set_colnames(c("resp", "var"))
  lm_uni <- lm(as.formula(glue("log(resp) ~ var")), data = df_uni) 
  pval <- coef(summary(lm_uni))[8] %>% round(digits = 3)
  est <- coef(summary(lm_uni))[2] %>% round(digits = 3)
  data.frame("pval" = pval,
       "est" = est)},
  USE.NAMES = TRUE, simplify = F) %>% bind_rows(.id="var")},
USE.NAMES = T, simplify = F) %>% bind_rows(.id = "ser") %>%
  filter(pval < 0.1) %>%
  kable

```


## Multivariable models
### correct for birthmode, exclusive breastfeeding and include interaction, sex, pets, ab first months 
```{r}
# menc
lm(log_menc_18m ~ bottle_excl + vag_b + sex + ab_m3 + pets + bottle_excl:vag_b + poly(d_menc_hv18m, 2), data = meta_logmenc_df %>% filter(!is.na(d_menc_hv18m))) %>% put(lm_menc_prag3) %>% tidy(conf.int = TRUE)

# Ps6b
lm(log_ps6b_12m ~ bottle_excl + vag_b + sex + ab_m3 + pets + bottle_excl:vag_b + poly(d_pcv11m_hv12m, 2), data = meta_logpcv_df %>% filter(!is.na(d_pcv11m_hv12m))) %>% put(lm_ps6b_prag3) %>% tidy(conf.int = TRUE)

```

## save dataframes
```{r}
save(meta_logmenc_df, meta_logpcv_df, file = here::here("RData", "dfs_meta_logigg.RData"))
```

## Validate for other pneumococcal serotypes
```{r}
df_meta_val <- sapply(c("average_Ps1_12m", "average_Ps4_12m", "average_Ps5_12m", "average_Ps7f_12m", "average_Ps9v_12m",
         "average_Ps14_12m", "average_Ps18c_12m", "average_Ps19f_12m", "average_Ps23f_12m"), function(RESP) {
  df_tmp <- meta_pcv_df %>%
    dplyr::select(RESP, bottle_excl, vag_b, sex, ab_m3, pets, d_pcv11m_hv12m) %>%
    dplyr::rename(resp = RESP) %>%
    drop_na(d_pcv11m_hv12m)
lm(log1p(resp) ~ bottle_excl + vag_b + sex + ab_m3 + pets + bottle_excl:vag_b + poly(d_pcv11m_hv12m, 2), data = df_tmp) %>% 
  tidy(conf.int = T) %>%
  filter(!grepl("poly", term)) %>%
  mutate(across(c(estimate, conf.low, conf.high), ~s(.x, digits = 2)),
         coef = glue("{estimate} ({conf.low}-{conf.high})"),
         p.value = ifelse(p.value < 0.001, "<0.001", s(p.value, digits = 3))) %>%
  dplyr::select(-c(std.error, statistic, estimate, conf.low, conf.high)) %>%
  relocate(p.value, .after = coef) %>%
  t() %>%
  as.data.frame() %>%
  row_to_names(row_number = 1) %>%
  rownames_to_column("par") %>%
  mutate(resp = RESP) %>%
  pivot_wider(id_cols = resp, names_from = par, values_from = `(Intercept)`:`bottle_excl1:vag_b1`)}, 
USE.NAMES = T, simplify = F) %>%
  bind_rows

write_delim(df_meta_val, here::here("results", subdir_name, "tab_meta_val.txt"), delim = ";")

```


# Forest plots
Interpretation of coefficients when the outcome is log-transformed:
https://stats.idre.ucla.edu/other/mult-pkg/faq/general/faqhow-do-i-interpret-a-regression-model-when-some-variables-are-log-transformed/
- The intercept is the **log of the geometric mean** when the explanatory variables are at the reference category
- The coefficients are the log of the ratio between the geometric mean of variable vs reference category
So here: the coefficients are the log of eg vaginal birth vs ceasarean. 
With the antilog (exp), we find the geometric mean ratio.

## The more pragmatic approach: in one figure with only Ps6b and menc
```{r fp_model}
df_prag3 <- data.frame(group = c(rep("ps6b", 9), rep("menc", 9)),
                       var = rep(names(coef(lm_menc_prag3)), 2),
                       est = c(coef(lm_ps6b_prag3) %>% unname, coef(lm_menc_prag3) %>% unname),
                       lower = c(confint(lm_ps6b_prag3)[, "2.5 %"] %>% unname,
                                 confint(lm_menc_prag3)[, "2.5 %"] %>% unname),
                       upper = c(confint(lm_ps6b_prag3)[, "97.5 %"] %>% unname,
                                 confint(lm_menc_prag3)[, "97.5 %"] %>% unname),
                       sig = c(coef(summary(lm_ps6b_prag3))[, "Pr(>|t|)"],
                               coef(summary(lm_menc_prag3))[, "Pr(>|t|)"])) %>%
  filter(!grepl("poly", var)) %>%
  mutate(sig2 = symnum(sig, cutpoints = c(0,  .001,.01,.05, 1),
                       symbols = c("***","**","*"," ")),
         group = factor(group),
         var = factor(var, levels = rev(names(coef(lm_menc_prag3))), ordered = TRUE))
 

fp_labels3 <- c("(Intercept)" = "Intercept",
                "bottle_excl1" = "Formula fed vs breastfed",
                "vag_b1" = "Vaginally born vs C-section born",
                "sex2" = "Female vs male",
                "ab_m31" = "AB vs no AB in first 3 months",
                "AB_courses" = "No. of antibiotic courses",
                "AB_courses_m18" = "No. of antibiotic courses",
                "pets1" = "Pets vs no pets in household",
                "bottle_excl1:vag_b1" = "Formula fed * Vaginally born")

fp_prag3 <- ggplot(data = df_prag3, aes(x=group, y=est, ymin=lower, ymax=upper)) +
  geom_errorbar(width = 0.25, aes(group = var), position = position_dodge(width=0.75)) +
  geom_point(size=3, aes(group=var, color=var), position = position_dodge(width=0.75)) +
  geom_text(aes(y=4,label=sig2,group=var), size=4, position=position_dodge(width = 0.75)) +
  geom_hline(yintercept=0, lty=2) +  # add a dotted line at x=0 after flip
  coord_flip()+   # flip coordinates (puts labels on y axis)
  ylab("Model coefficient") +  
  #scale_x_discrete(labels = as_labeller(fp_labels)) +
  scale_color_manual(values = brewer.pal(7, "Dark2"), 
                     name = "Covariate", 
                     labels = as_labeller(fp_labels3)) +
  scale_x_discrete(labels = c("MenC", "Ps6B")) +
  guides(color=guide_legend(reverse = TRUE)) +
  theme_pubr() + 
  theme(plot.title = element_text(size=15),
        #axis.text.y = element_text(size=12),
        axis.title.y = element_blank(),
        #axis.title=element_text(size=12,face="bold"),
        legend.text = element_text(),
        legend.position = "right")

fp_prag3

df_prag3 %>%
  write_source_data("Fig2", "a")
```

# Stratified

## dataframe
```{r}
meta_all_df <- meta_pcv_df %>%
  left_join(., meta_menc_df %>% dplyr::select(dn_nr, d_menc_hv18m, average_menc_18m), by = "dn_nr") %>%
  mutate(group1 = case_when(vag_b == "1" & bottle_excl == "0" ~ "vb_bf",
                           vag_b == "1" & bottle_excl == "1" ~ "vb_bottle",
                           vag_b == "0" & bottle_excl == "0" ~ "cs_bf",
                           vag_b == "0" & bottle_excl == "1" ~ "cs_bottle") %>% 
           factor(levels = c("vb_bf", "vb_bottle", "cs_bf", "cs_bottle"), ordered = T))

# numbers in each group
meta_all_df %>%
  drop_na(average_Ps6b_12m) %>%
  group_by(group1) %>% 
  summarise(n=n())

# numbers in each group
meta_all_df %>%
  drop_na(average_menc_18m) %>%
  group_by(group1) %>% 
  summarise(n=n())

```


## stats: ANOVA followed by Tukey-Kramer test
```{r}
# anova
anova_grp1 <- sapply(c("average_menc_18m", "average_Ps6b_12m", 
                       "average_Ps7f_12m", "average_Ps14_12m"),
       function(RESP) {
         DAYS <- ifelse(substr(RESP, 9, 12) == "menc", "d_menc_hv18m", "d_pcv11m_hv12m")
         df_tmp <- meta_all_df %>%
           dplyr::select(RESP, DAYS, group1) %>%
           set_colnames(c("resp", "days", "group1")) %>%
           na.omit
         fit <- lm(log(resp) ~ group1 + poly(days, 2), data = df_tmp)
         anova(fit)["group1", "Pr(>F)"]
         },
       USE.NAMES = T, simplify = F) %>%
  bind_cols %>%
  t() %>%
  data.frame %>%
  set_colnames("pval")

```

## post hoc stats
https://statsandr.com/blog/anova-in-r/#normality
```{r}
aov_ps6b <- aov(log(average_Ps6b_12m) ~ group1 + poly(d_pcv11m_hv12m, 2), data = meta_all_df %>% drop_na(d_pcv11m_hv12m))
summary(aov_ps6b)
# visualize normal distribution of residuals
hist(aov_ps6b$residuals)
qqPlot(aov_ps6b$residuals)
# test for equal variances...
LeveneTest(log(average_Ps6b_12m) ~ group1, data = meta_all_df %>% drop_na(d_pcv11m_hv12m))
# cannot reject null hypothesis that variances are equal between groups

# Tukey-Kramer test = method to account for multiple testing that can account for unbalanced designs. Pvalues are adjusted for multiple comparisons.
HSD.test(aov_ps6b, "group1", console=T, group=F, unbalanced=T)

# MenC: only mode of birth.
aov_menc <- aov(log(average_menc_18m) ~ vag_b + poly(d_menc_hv18m, 2), data = meta_all_df %>% drop_na(d_menc_hv18m))
summary(aov_menc)
# visualize normal distribution of residuals
hist(aov_menc$residuals)
qqPlot(aov_menc$residuals)
# test for equal variances
LeveneTest(log(average_menc_18m) ~ vag_b, data = meta_all_df %>% drop_na(d_menc_hv18m))

```

## plot
```{r gg_ps6b, w=8}
# calc gmc
df_gmc_grp <- sapply(levels(meta_all_df$group1), function(grp) {
                        df_tmp <- meta_all_df %>%
                          dplyr::select(average_Ps6b_12m, group1) %>%
                          set_colnames(c("resp", "group1")) %>%
                          na.omit
                        geo.ci.fun(df_tmp %>% filter(group1 == grp) %>% pull(resp))}, 
                        USE.NAMES = TRUE, simplify = TRUE) %>% 
                        t() %>%
                        as.data.frame %>%
                        rownames_to_column("group1") %>%
                        mutate(resp = "average_Ps6b_12m") %>%
  mutate(group1=factor(group1))

# annotations
df_lines_grp <- data.frame("resp" = c("average_Ps6b_12m", "average_Ps6b_12m"),
           x = c(1,2),
           y = c(rep(500, 2)),
           no = c(1,1) %>% factor)

df_ann_grp <- data.frame("resp" = c("average_Ps6b_12m"),
           x = c(1.5),
           y = c(800),
           label = c("p.adj=0.070"))

# plot
ggs_bmf <- sapply(c("average_Ps6b_12m"),
       function(RESP) {
         df <- meta_all_df %>%
           dplyr::select(RESP, group1) %>%
           set_colnames(c("resp", "group1")) %>%
           na.omit 
         
         ggplot(df, aes(x=group1, y=resp)) + 
           geom_jitter(aes(color=group1), width = .2, alpha = 0.8) +
           geom_point(data = df_gmc_grp %>% filter(resp == RESP), 
                      aes(x=group1, y=y), size = 3) +
           geom_errorbar(data = df_gmc_grp %>% filter(resp == RESP), 
                         aes(x=group1,  y=y, ymin=ymin, ymax=ymax), 
                         width = 0.4) +
           geom_line(data = df_lines_grp %>% filter(resp == RESP),
                     aes(x=x,y=y,group=no)) +
           #geom_text(data = df_ann_grp %>% filter(resp == RESP),
          #           aes(x=x,y=y,label=label)) +
           scale_color_manual(values = c("#2c7bb6", "#abd9e9", "#d7191c", "#fdae61"),
                              labels = c("Vaginally born\nbreastfed",
                                         "Vaginally born\nformula fed",
                                         "C-section born\nbreastfed",
                                         "C-section born\nformula fed"),
                              name = "") +
         scale_y_continuous(trans = "log10", breaks = c(0.01, 0.1, 1, 10, 100, 1000), 
                            labels = c("0.01", "0.1", "1", "10", "100", "1,000"), limits = c(0.01, 6000)) +
           scale_x_discrete(labels = c(paste0("vag+bf<br>*n*=", 
                                              nrow(df %>% filter(group1 == "vb_bf"))), 
                                       paste0("vag+ff<br>*n*=", 
                                              nrow(df %>% filter(group1 == "vb_bottle"))),
                                       paste0("cs+bf<br>*n*=",
                                              nrow(df %>% filter(group1 == "cs_bf"))),
                                       paste0("cs+ff<br>*n*=",
                                              nrow(df %>% filter(group1 == "cs_bottle"))))) +
           ylab(paste0("Anti-", case_when(RESP == "average_menc_18m" ~ "MenC", 
                                          RESP == "average_Ps6b_12m" ~ "Ps6B",
                                          RESP == "average_Ps7f_12m" ~ "Ps7F",
                                          RESP == "average_Ps14_12m" ~ "Ps14"), 
                       " IgG (ng/ml)")) +
           geom_richtext(data=data.frame(x=c(1.5, 1.5), y=c(0.015, 850), label = c("   ANOVA: *F*<sub>3,95</sub> = 3.1, *p* = 0.031", "*p<sub>adj</sub>* = 0.070")),
                         aes(x=x,y=y, label=label), label.colour=NA, fill=NA, size =2.5) +
           #annotate(geom = "text",
          #          x=0.85, y=0.01,
          #     label = paste0("   ANOVA: F(3,97)=3.1, p=", s(anova_grp1[RESP, "pval"], digits = 3)),
          #     hjust = 0.25, vjust = -0.5
          #     ) +
           theme_pubr() +
           guides(color = guide_legend(override.aes = list(size = 2.5))) +
           theme(axis.title.x = element_blank(),
                 axis.text.x = element_markdown())
         },
       USE.NAMES = T, simplify = F)

ggs_bmf$average_Ps6b_12m

```

## For MenC: only mode of birth
```{r ggs_strat_modeofbirth, w=4}
df_gmc_mob <- sapply(c("1", "0"), function(grp) {
                        df_tmp <- meta_all_df %>%
                          dplyr::select(average_menc_18m, vag_b) %>%
                          na.omit
                        geo.ci.fun(df_tmp %>% filter(vag_b == grp) %>% pull(average_menc_18m))}, 
                        USE.NAMES = TRUE, simplify = TRUE) %>% 
                        t() %>%
                        as.data.frame %>%
                        rownames_to_column("vag_b") %>%
                        mutate(resp = "average_menc_18m") %>%
  mutate(vag_b=factor(vag_b, levels = c("1", "0")))

gg_mob <- ggplot(meta_all_df %>% mutate(vag_b = fct_rev(vag_b)), aes(x=vag_b, y=average_menc_18m)) + 
           geom_jitter(aes(color=group1), width = .2, alpha = 0.8) +
           geom_point(data = df_gmc_mob, 
                      aes(x=vag_b, y=y), size = 3) +
           geom_errorbar(data = df_gmc_mob, 
                         aes(x=vag_b,  y=y, ymin=ymin, ymax=ymax), 
                         width = 0.4) +
           scale_color_manual(values = c("#2c7bb6", "#abd9e9", "#d7191c", "#fdae61"),
                              labels = c("Vaginally born\nbreastfed",
                                         "Vaginally born\nformula fed",
                                         "C-section born\nbreastfed",
                                         "C-section born\nformula fed"),
                              name = "") +
         scale_y_continuous(trans = "log10", breaks = c(0.01, 0.1, 1, 10, 100, 1000), 
                            labels = c("0.01", "0.1", "1", "10", "100", "1,000"), limits = c(0.01, 6000)) +
  scale_x_discrete(labels = c(paste0("vag<br>*n*=", nrow(meta_all_df %>% drop_na(average_menc_18m) %>% filter(vag_b == "1"))), 
                              paste0("cs<br>*n*=", nrow(meta_all_df %>% drop_na(average_menc_18m) %>% filter(vag_b == "0"))))) +
           ylab("Anti-MenC IgG (ng/ml)")+
  geom_richtext(data=data.frame(x=c(1.5), y=c(0.015), label = c("   ANOVA: *F*<sub>1,62</sub> = 7.3, *p* = 0.002")),
                         aes(x=x,y=y, label=label), label.colour=NA, fill=NA, size =2.5) +
           #annotate(geom = "text",
          #          x=0.85, y=0.01,
          #     label = "   ANOVA: p=0.002",
          #     hjust = 0.25, vjust = -0.5
          #     ) +
           theme_pubr() +
           guides(color = guide_legend(override.aes = list(size = 3))) +
           theme(axis.title.x = element_blank(),
                 axis.text.x=  element_markdown())

gg_mob
```



## combine plots
```{r fig_meta, h=7, w=6.125}
ggarrange(fp_prag3 + theme(text = element_text(size=9)),
          ggarrange(ggs_bmf$average_Ps6b_12m + theme(text = element_text(size=9)), gg_mob + theme(text = element_text(size=9)),
          common.legend = TRUE, widths = c(3/5, 2/5)), labels = c("a", "b"), nrow = 2)
```
## Source data Fig 2b
```{r}
meta_all_df %>%
  dplyr::select(dn_nr, average_menc_18m, average_Ps6b_12m, vag_b, group1) %>%
  filter(!(is.na(average_Ps6b_12m) & is.na(average_menc_18m))) %>% 
  write_source_data("Fig2", "b")
```


## fold changes
```{r}
df_gmc_grp %>%
  dplyr::select(-ymin, -ymax) %>%
  pivot_wider(names_from = group1,
              values_from = y) %>%
  mutate(FC_vbbf_csbf = vb_bf/cs_bf,
         FC_vbbf_vbbottle = vb_bf/vb_bottle) %>%
  column_to_rownames("resp") %>% 
  round(digits = 3) %>%
  kable

df_gmc_mob %>%
  dplyr::select(-ymin, -ymax) %>%
  pivot_wider(names_from = vag_b,
              values_from = y) %>%
  mutate(FC_vb_cs = `1`/`0`)
```


```{r}
sessionInfo()
```

