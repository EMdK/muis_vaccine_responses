Author: Emma M. de Koff, Date: February 1, 2022

This repository accompanies the manuscript "Mode of delivery modulates the intestinal microbiota and impacts the response to vaccination".
References to the sequencing data and the participant metadata underlying the figures can be found under "Data availability" in the manuscript. Full participant metadata can be obtained upon request and approval of a proposal. Please contact d.bogaert@ed.ac.uk. 

The folder /functions contains custom R functions that are in part used in the scripts.

The folder /scripts contains all R scripts that were used to create the figures and tables, including supplementary figures and tables, in the manuscript. Scripts were run in R version 4.0.3.

- `correlate_vacc_response.Rmd`: used to calculate correlations between antibody responses to different vaccine serotypes and to create Fig. 1
- `2_phyloseq.Rmd`: used to compile all data into one phyloseq object
- `3_meta_influences.Rmd`: used to evaluate associations between host metadata and antibody responses, and to create baseline Table 1, Supplementary Figure 1, Fig. 2 and Supplementary Table 1
- `4_alpha_beta.Rmd`: used to assess associations between alpha and beta diversity and antibody responses, and to create Supplementary Table 2
- `5_DMM.Rmd`: determine community state types using Dirichlet multinomial mixture models, and to create Fig. 3, Fig. 4, Supplementary Fig. 2, Supplementary Fig. 3, Supplementary Fig. 4, Supplementary Table 3, and Supplementary Table 4.
- `6_fitTimeSeries.Rmd`: used to create tables from smoothing spline ANOVA analysis results obtained with script `6b_fTS_t12m_hpc.Rmd`: Supplementary Table 5, Supplementary Table 6, and Supplementary Table 7
- `6b_fTS_t12m_hpc.Rmd`: used to run smoothing spline ANOVA to detect differentially abundant OTUs. Same analysis was also run to include only the first 2 months of life.
- `8_qPCR.Rmd`: used to study species-specific detection by quantitative PCR in relation to abundances obtained by 16S rRNA gene based sequencing and to antibody responses to vaccination.
